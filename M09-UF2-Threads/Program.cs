﻿/*
 * Objectiu: presentació de la classe Thread i el seu ús bàsic
 * Autor: Eduard Ruesga Cortiella
 * 
 */

using System;
using System.IO;
using System.Threading;

namespace M09_UF2_Threads
{
    class Program
    {
        public static bool runBool;
         static void Main(string[] args)
        {
            //escriu una funció que obri un fitxer de text i compti quantes línies té.
            //Console.WriteLine(CountFileLines(@"..\..\..\Fitxer1.txt"));

            //crea un Thread que compti i mostri quantes línies té un fitxer (reutilitza el codi anterior). El nom del fitxer serà un atribut del Thread de tipus String.
            //OneOnlyThread();

            //Crea una classe de Prova que crida a 3 threads per esbrinar quantes línies té cada fitxer. Proveu amb fitxers grans i petits.
            TwoThreads();

            Console.ReadKey();

        }

        //Funció que obre un fitxer de text i compta quantes línies té.
        static int CountFileLines(string filePath)
        {
            using (StreamReader readFile = new StreamReader(filePath))
            {
                int lines = 0;
                while (readFile.ReadLine() != null) { 
                    lines++;
                    Console.WriteLine(lines);
                }
                return lines;
            }
        }
        

        static void OneOnlyThread()
        {
            Thread myFirstThread = new Thread(MyThreadFunc);

            Console.WriteLine("La implementació normal és un sol fil");
            myFirstThread.Start();

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Aquesta linia està en el progrés principal");

            /***** Pas de paràmetre en un thread***/
            Thread mySecondThreadWithParam = new Thread(funcThreadParam);
            mySecondThreadWithParam.Start("Fitxer1.txt");
        }

        static void MyThreadFunc()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("La implementació normal és un sol fil");
        }

        static void funcThreadParam(object o)
        {
            string s = Convert.ToString(o);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("El nom és: " + s);
            Console.WriteLine("Nº de linias: " + CountFileLines(@"..\..\..\" + s));


        }


        private static void TwoThreads()
        {
            string fitxer1 = "Fitxer1.txt";
            int m = 0;
            int x = 0;

            runBool = true;

            /*Thread[] aThreads = new Thread[1];
            for (int i=0; i< aThreads.Length; i++)
            {
                aThreads[i] = new Thread(ThreadMessage);
                aThreads[i].Start("Fitxer2.txt");
            }*/

            Thread aThreads = new Thread(ThreadMessage);
            aThreads.Start("Fitxer2.txt");

            Thread bThreads = new Thread(ThreadMessage1);
            bThreads.Start("Fitxer3.txt");

            while (runBool)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                //Console.WriteLine($"This is the main thread! Iteration: {m}");
                Console.WriteLine("Nº de linias del fitxer" + fitxer1 + ": " + CountFileLines(@"..\..\..\" + fitxer1));
                //m++;
            }
        }

        static void ThreadMessage(object o)
        {
            string s = Convert.ToString(o);
            int n = 0;

            while (runBool) { 
                Console.ForegroundColor = ConsoleColor.Green;
                //Console.WriteLine($"This is the Thread number: {o} iteration: {n}");
                //n++;
                
                Console.WriteLine("Nº de linias del fitxer" + s + ": " + CountFileLines(@"..\..\..\" + s));
            }

        }

        static void ThreadMessage1(object o)
        {
            string s = Convert.ToString(o);
            int n = 0;

            while (runBool)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                //Console.WriteLine($"This is the Thread number: {o} iteration: {n}");
                //n++;

                Console.WriteLine("Nº de linias del fitxer" + s + ": " + CountFileLines(@"..\..\..\" + s));
            }

        }

    }
}
